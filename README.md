Game piece in robot prior to match^cargo^hatch|3
Platform robot started^high^medium^low^ground|3
Did the robot leave the hab?^yes^no|3
Where did they deliver the hatch/cargo?^Rocket (Left)^Rocket (Right)^Cargo|3
What kind of movement?^auto^teleop^none|3
Hatches scored? (Cargo)|1
Hatches scored? (Left Rocket)|1
Hatches scored? (Right Rocket)|1
Cargo scored? (Cargo)|1
Cargo scored? (Left Rocket)|1
Cargo scored? (Right Rocket)|1
ENDGAME|5
Where did the robot finish the match^out of hab^platform^medium^high|3